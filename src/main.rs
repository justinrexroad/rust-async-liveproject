use async_std::prelude::*;
use async_std::stream;
use async_trait::async_trait;
use chrono::prelude::*;
use clap::Clap;
use std::{
    fmt::{Display, Formatter},
    fs::File,
    io::{BufWriter, Write},
    time::Duration,
};
use xactor::*;
use yahoo_finance_api as yahoo;

mod signal;
use signal::{AsyncStockSignal, MaxPrice, MinPrice, PriceDifference, WindowedSMA};

#[derive(Clap)]
#[clap(
    version = "1.0",
    author = "Claus Matzinger",
    about = "A Manning LiveProject: async Rust"
)]
struct Opts {
    #[clap(short, long, default_value = "AAPL,MSFT,UBER,GOOG")]
    symbols: String,
    #[clap(short, long)]
    from: String,
}

#[message]
#[derive(Debug, Default, Clone)]
struct Quotes {
    pub symbol: String,
    pub quotes: Vec<yahoo::Quote>,
}

#[message]
#[derive(Debug, Clone)]
struct QuoteRequest {
    symbol: String,
    beginning: DateTime<Utc>,
    end: DateTime<Utc>,
}

#[message]
#[derive(Clone)]
struct Signals {
    timestamp: DateTime<Utc>,
    symbol: String,
    price: f64,
    change: f64,
    min: f64,
    max: f64,
    sma: f64,
}

impl Display for Signals {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{},{},${:.2},{:.2}%,${:.2},${:.2},${:.2}",
            self.timestamp,
            self.symbol,
            self.price,
            self.change * 100.0,
            self.min,
            self.max,
            self.sma
        )
    }
}

#[derive(Default)]
struct Downloader {
    provider: yahoo::YahooConnector,
}

impl Downloader {
    fn new(provider: yahoo::YahooConnector) -> Self {
        Downloader { provider }
    }
}

#[async_trait]
impl Actor for Downloader {
    async fn started(&mut self, ctx: &mut Context<Self>) -> Result<()> {
        ctx.subscribe::<QuoteRequest>().await
    }
}

#[async_trait]
impl Handler<QuoteRequest> for Downloader {
    async fn handle(&mut self, _ctx: &mut Context<Self>, msg: QuoteRequest) {
        let data = match self
            .provider
            .get_quote_history(&*msg.symbol, msg.beginning, msg.end)
            .await
        {
            Ok(response) => {
                if let Ok(quotes) = response.quotes() {
                    Quotes {
                        symbol: msg.symbol,
                        quotes: quotes,
                    }
                } else {
                    Quotes {
                        symbol: msg.symbol,
                        quotes: vec![],
                    }
                }
            }
            Err(e) => {
                eprintln!("API Error for symbol {}, {}", msg.symbol, e);
                Quotes {
                    symbol: msg.symbol,
                    quotes: vec![],
                }
            }
        };
        if let Err(e) = Broker::from_registry().await.unwrap().publish(data) {
            eprintln!("Broker error, {}", e);
        }
    }
}

struct FileWriter {
    filename: String,
    writer: Option<BufWriter<File>>,
}

impl FileWriter {
    fn new(filename: String) -> Self {
        FileWriter {
            filename,
            writer: None,
        }
    }
}

#[async_trait]
impl Actor for FileWriter {
    async fn started(&mut self, ctx: &mut Context<Self>) -> Result<()> {
        let mut file = File::create(&self.filename)
            .unwrap_or_else(|_| panic!("Could not open target file '{}'", self.filename));
        let _ = writeln!(
            &mut file,
            "period start,symbol,price,change %,min,max,30d avg"
        );
        self.writer = Some(BufWriter::new(file));
        ctx.subscribe::<Signals>().await
    }
    async fn stopped(&mut self, ctx: &mut Context<Self>) {
        if let Some(writer) = &mut self.writer {
            writer
                .flush()
                .expect("Something happened when flushing. Data loss :(")
        };
        ctx.stop(None);
    }
}

#[async_trait]
impl Handler<Signals> for FileWriter {
    async fn handle(&mut self, _ctx: &mut Context<Self>, msg: Signals) {
        if let Some(file) = &mut self.writer {
            let _ = writeln!(file, "{}", msg);
        }
    }
}

#[derive(Clone, Default)]
struct Processor;

#[async_trait]
impl Actor for Processor {
    async fn started(&mut self, ctx: &mut Context<Self>) -> Result<()> {
        ctx.subscribe::<Quotes>().await
    }
}

#[async_trait]
impl Handler<Quotes> for Processor {
    async fn handle(&mut self, _ctx: &mut Context<Self>, mut msg: Quotes) {
        let data = msg.quotes.as_mut_slice();

        if !data.is_empty() {
            // Sort by timestamp to ensure properly ordered.
            data.sort_by_cached_key(|k| k.timestamp);
            let closes: Vec<f64> = data.iter().map(|q| q.adjclose as f64).collect();
            let diff = PriceDifference {};
            let min = MinPrice {};
            let max = MaxPrice {};
            let sma = WindowedSMA { window_size: 30 };

            let period_max: f64 = max.calculate(&closes).await.unwrap_or(0.0);
            let period_min: f64 = min.calculate(&closes).await.unwrap_or(0.0);

            let last_price = *closes.last().unwrap();
            let (_, pct_change) = diff.calculate(&closes).await.unwrap_or((0.0, 0.0));
            let sma = sma.calculate(&closes).await.unwrap();

            let data = Signals {
                timestamp: Utc.timestamp(data.last().unwrap().timestamp as i64, 0),
                symbol: msg.symbol,
                price: last_price,
                change: pct_change,
                min: period_min,
                max: period_max,
                sma: *sma.last().unwrap_or(&0.0),
            };
            println!("{}", data);
            if let Err(e) = Broker::from_registry().await.unwrap().publish(data) {
                eprintln!("Broker error, {}", e);
            }
        }
    }
}

#[xactor::main]
async fn main() -> Result<()> {
    let opts = Opts::parse();
    let symbols: Vec<String> = opts.symbols.split(',').map(|s| s.to_owned()).collect();
    let from: DateTime<Utc> = opts.from.parse().expect("Couldn't parse 'from' date");

    let _addr1 = Supervisor::start(|| Downloader::new(yahoo::YahooConnector::new())).await?;
    let _addr2 = Supervisor::start(|| Processor::default()).await?;
    let _addr3 =
        Supervisor::start(|| FileWriter::new(format!("{}.csv", Utc::now().to_rfc2822()))).await?;

    // a simple way to output a CSV header
    println!("period start,symbol,price,change %,min,max,30d avg");

    let mut interval = stream::interval(Duration::from_secs(30));

    while let Some(_) = interval.next().await {
        let to = Utc::now();

        for symbol in &symbols {
            if let Err(e) = Broker::from_registry().await?.publish(QuoteRequest {
                symbol: String::from(symbol),
                beginning: from,
                end: to,
            }) {
                eprintln!("Broker Error: {}", e)
            }
        }
    }

    Ok(())
}
